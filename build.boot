(set-env!
 :source-paths #{"src"}
 :resource-paths #{"resources" "assets"}
 :dependencies '[[perun "0.3.0"]
                 [hiccup "1.0.5"]
                 [garden "1.3.2"]
                 [pandeiro/boot-http "0.7.3"]
                 [jeluard/boot-notify "0.1.2" :scope "test"]
                 [cpmcdaniel/boot-copy "1.0"]
                 [clj-http "2.3.0"]
                 [org.clojure/data.json "0.2.6"]])

(require '[io.perun :refer :all]
         '[io.perun.core :as perun]
         '[ok.index :as index-view]
         '[ok.post :as post-view]
         '[pandeiro.boot-http :refer [serve]]
         '[garden.core :refer [css]]
         '[cpmcdaniel.boot-copy :refer :all]
         '[clojure.data.json]
         '[clj-http.client])
;;'[jeluard.boot-notify :refer [notify]]

(deftask slack
  "Post `message` to slack."
  [u url URL str "The slack incoming webhook url"
   m message MESSAGE str "The slack message"]
  (with-post-wrap fileset
    (let [data {:username "GitLab CI"
                :text message
                :icon_emoji ":gitlabci:"}
          json (clojure.data.json/write-str data)]
      (clj-http.client/post url {:body json}))))

(task-options!
 slack {:url "https://hooks.slack.com/services/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"}
 copy {:output-dir "target/public"
       ;; TODO: Make this regexp more readable. It has three parts:
       ;;       google search console + favicon stuff + other assets.
       :matching   #{#"(google.*\.html|safari-pinned-tab\.svg|favicon\.ico|browserconfig\.xml|manifest\.json)|\.(css|js|png|jpg)$"}})

(defn slug-fn
  "Slugs are derived from filenames of markdown files. They can have a
  YYYY-MM-DD- prefix or not."
  [filename]
  (last (re-find #"(\d+-\d+-\d+-|)(.*)\.md" filename)))

(deftask set-meta-data
  "Add :key attribute with :val value to each file metadata and also
   to the global meta"
  [k key VAL kw "meta-data key"
   v val VAL str "meta-data value"]
  (with-pre-wrap fileset
    (let [files           (perun/get-meta fileset)
          global-meta     (perun/get-global-meta fileset)
          updated-files   (map #(assoc % key val) files)
          new-global-meta (assoc global-meta key val)
          updated-fs      (perun/set-meta fileset updated-files)]
      (perun/set-global-meta updated-fs new-global-meta))))

(deftask categories
  "Add :categories of all posts to the meta-data"
  []
  (with-pre-wrap fileset
    (let [files           (perun/get-meta fileset)
          global-meta     (perun/get-global-meta fileset)
          categories      (filter #(:category %) files)
          updated-files   (map #(assoc % :categories categories) files)
          new-global-meta (assoc global-meta :categories categories)
          updated-fs      (perun/set-meta fileset updated-files)]
      (perun/set-global-meta updated-fs new-global-meta))))

(deftask build
  "Build the 200ok page."
  []
  (let [is-of-type? (fn [{:keys [path]} doc-type] (.startsWith path doc-type))]
    (comp
     (markdown)
     (draft)
     ;;(print-meta)
     (slug :slug-fn slug-fn)
     (ttr)
     (categories)
     (word-count)
     (permalink)
     (canonical-url)
     (build-date)
     (gravatar :source-key :author-email
               :target-key :author-gravatar)

     ;; renders each md file in posts into its own page
     (render :renderer 'ok.post/render
             :filterer #(is-of-type? % "posts"))

     (render :renderer 'ok.page/render
             :filterer #(is-of-type? % "audio-book"))

     (collection :renderer 'ok.person/render-collection
                 :page "team.html"
                 :filterer #(is-of-type? % "team"))

     (collection :renderer 'ok.index/render
                 :page "index.html"
                 ;; Order pages in reverse chronological order
                 :sortby #(:publish_at %)
                 :comparator #(.compareTo %2 %1)
                 :filterer #(is-of-type? % "posts"))

     (collection :renderer 'ok.index/render
                 :page "projects.html"
                 :filterer #(is-of-type? % "projects"))

     (collection :renderer 'ok.index/render
                 :page "open-source.html"
                 :filterer #(is-of-type? % "open-source"))

     ;; Groups all posts that have a :category (yes, only a single one
     ;; atm) into one file. For example a post with a :category of
     ;; "emacs" will be rendered into a file
     ;; "public/category/emacs.html" together with every other post
     ;; with the same tag.
     (collection :renderer 'ok.index/render
                 :groupby #(str (:category %) ".html")
                 :out-dir "public/category"
                 :filterer #(is-of-type? % "posts"))

     (inject-scripts :scripts #{"start.js"})
     (sitemap)
     ;;(rss :title "Hello"
     ;;     :description "Hashobject blog")
     (atom-feed); :out-dir "public")
     ;;(notify)
     ;;(print-meta)
     (target)
     (copy))))

(deftask dev
  []
  (comp
   (watch)
   (global-metadata)
   (set-meta-data :key :target
                  :val "dev")
   (build)
   (serve :dir "target/public")))

(deftask prod
  []
  (comp
   ;;(slack :message "<http://200ok.ch|200ok.ch> has been updated.")
   (global-metadata)
   (set-meta-data :key :target
                  :val "prod")
   (build)))
