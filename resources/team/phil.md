---
name: Phil Hofmann
email: phil@200ok.ch
image: /img/phil.jpg
position: 2
---

### Phil Hofmann, Founder

Phil has been programming for the web since 1999. He is an experienced
Software Engineer.
