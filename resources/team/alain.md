---
name: Alain M. Lafon
image: /img/alain.jpg
email: alain@200ok.ch
position: 1
---

### Alain M. Lafon, Founder & CEO

Alain M. Lafon has been programming for the web since
2004. He is an avid follower of Web Standards and constantly improving
his own productivity as well as the productivity of the team he is
working with by his profound knowledge about agile methodologies as
well as tooling to facilitate a smooth software development process.
