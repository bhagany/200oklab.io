---
name: "DSL resources for Ruby"
authors: "Alain M. Lafon, Phil Hofmann"
category: "ruby"
publish_at: 2016-12-10
tags: ruby, dsl, programming
---

This is nothing more than linked list of resources for learning how to
write DSLs with Ruby, but also DSLs in general.

(DSL stands for Domain Specific Language and which is a programming
language that is closely modelled after the domain it is used
in. [Wikipedia](https://en.wikipedia.org/wiki/Domain-specific_language) has
a good introductory article.)

[**Obie Fernandez**](http://obiefernandez.com/) wrote the reference on
Rails. In this podcast he speaks about what a DSL is, the difference
between internal and external DSLs as well as the importance of the
flexibly syntax of the host language in order to make DSLs worthwhile.

http://podbay.fm/show/120906714/e/1175932332

[**Martin Fowler**](http://www.martinfowler.com/) is a well known
expert in Software Engineering specializing in patterns. In this
podcast he talks with Rebecca Parsons about the definition of DSL,
Internal vs. External DSLs, reasons to use DSLs and reasons not to and
the DSL lifecycle.

http://www.se-radio.net/2012/01/episode-182-domain-specific-languages-with-martin-fowler-and-rebecca-parsons/

Last but not least, there is a Ruby library which let's you map a DSL
to your Ruby objects in a snap.

https://ms-ati.github.io/docile/
