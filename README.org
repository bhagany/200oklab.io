* 200ok Site

** Prerequisites

-  Install [[https://github.com/boot-clj/boot][boot]]

** Usage

Run =boot dev=.

This will start a web server on port 3000, watch the directory and
rebuild the target on changes.

On =git push= gitlab ci takes care of deployment.

*** Initial setup

- Add :base-url to global-meta data

** SEO

We are using [[https://schema.org][schema.org]] markup to add some SEO juice. When adding new
structured data, test it out on [[https://search.google.com/structured-data/testing-tool][Googles Structured Testing Tool]]. Find
out more about structured data on Googles [[https://developers.google.com/search/docs/guides/intro-structured-data][Introduction to Structured Data]] article.

** Publish Guideline

*** Posts

1. Create a new .md file within /posts
2. Add required meta data for
   - name
   - category
     - Note: Categories are also used in navigation. They define the
       'main interests' on our blog. Therefore it's not bad if we
       regularly come back to the same categories. The category is
       also our "focus keyword" for the post.
   - tags
     - Use as many as you like. They help with SEO.
     - At the moment, tags and category will be merged as "keywords"
       for SEO. Do not repeat the category within the tags, because
       there is no 'unique' filter atm.
   - publish_at (Format YYYY-MM-DD)

**** Checks

- If you used a new Category within your Post, make sure to also link
  it in the [[file:src/ok/index.clj][index.clj]] layout.

** Scale Images (with Image Magick)

E.g. =mogrify -geometry 200x resources/img/nick.jpg=

** Generate a Word Cloud

see https://www.jasondavies.com/wordcloud/

Here is a word list to get started with

Clojure
ClojureScript
Reagent
Figwheel
re-frame
ReactNative
re-natal
Hiccup
Compjure
CompojureAPI
Garden
Selmer
Luminus
Ring
hugsql

TODO generate the wordlist from project metadata

** Directoy Structure

#+BEGIN_SRC
├── assets             - static web assets which will be copied verbatim
├── build.boot         - the boot config
├── README.org         - this file
├── resources          - where all the markdown files go
│   ├── audio-book     - pages for the audio book
│   ├── open-source    - pages for open source projects/contributions
│   ├── perun.base.edn - global metadata file
│   ├── posts          - where all the blog articles go
│   ├── projects       - pages for projects
│   └── team           - the team member directory
├── src                - custom clojure code, mostly hiccup templates
└── target
    └── public         - where the generated site ends up
#+END_SRC

** TODOs

The only thing I'd change is that `permalink` and `canonical-url` are no longer necessary - resolution of those keys are automatic now.

*** TODO Upgrade to new Perun version

- [ ] =permalink= and =canonical-url= are no longer necessary - resolution of those keys are automatic now

*** TODO Implement tags

- [X] There is now a =tags= task in Perun
